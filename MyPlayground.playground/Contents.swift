import UIKit

//create doubly linked list

class Node<T> {
    
    var value: T
    var next: Node<T>?
    weak var prev: Node<T>?
    
    init(value: T) {
        self.value = value
    }
}

class DoublyLinkedList<T> {
    
    fileprivate var head: Node<T>?
    private var tail: Node<T>?
    public var list: [Node<T>?] = []
    
    public var isEmpty: Bool {
      return head == nil
    }

    public var first: Node<T>? {
      return head
    }

    public var last: Node<T>? {
      return tail
    }
    
    //CHECK
//    public var length: Int {
//        get {
//        print(list.count)
//        return list.count
//        }
//    }
    
    var length: Int{

           
               var count = 0
               var current = self.head
               while current != nil{

                   count += 1
                   current = current?.next
               }
               return count
           
       }
    
    
    //add function
    public func add(_ value: T) {
        
        let newNode = Node(value: value)
        
        if let tailNode = tail {
            newNode.prev = tailNode
            tailNode.next = newNode
        }
        else {
            head = newNode
        }
        tail = newNode
    }
    
    
    //Find index function
    public func node(index: Int) -> Node<T>? {
      
      if index >= 0 { // checks index is not zero
        var node = head
        var i = index
        
        while node != nil { // return specific index
          if i == 0 { return node }
          i -= 1
          node = node!.next
        }
      }
      
      return nil // return nil for any other events
    }
    
    
    //remove function
    public func remove(node: Node<T>) -> Any {
      let prev = node.prev
      let next = node.next

      if let prev = prev {
        prev.next = next
      } else {
        head = next
      }
      next?.prev = prev

      if next == nil {
        tail = prev
      }

      node.prev = nil
      node.next = nil

      return node.value
    }


}




//create stack
class Stack<T> {
    
    let doubleLinkedList = DoublyLinkedList<T>()
    
    var size: Int {
        doubleLinkedList.length
    }

    var peek: Node<T>? {
        return doubleLinkedList.first
//        print(super.last)
    }

    func push(_ element: T) {
//        list.append(element)
        doubleLinkedList.add(element)
    }

    func pop() -> Any? {
        return doubleLinkedList.remove(node: doubleLinkedList.last!)
//        return list
    }
}



extension DoublyLinkedList: CustomStringConvertible {

  public var description: String {

    var text = "H-"
    var nodeList = head

    while nodeList != nil {
        text += "[\(nodeList!.value)]"
        nodeList = nodeList?.next
      if nodeList != nil { text += "-" }
    }

    return text + "-T"
  }
}


extension Node: CustomStringConvertible {

    public var description: String {

        let  textNode = "[\(value)]"
        return  textNode
    }
}

extension Stack: CustomStringConvertible {
    
   public var description: String {

      var stackText = "["
    var stackList = doubleLinkedList.head

      while stackList != nil {
        stackText += "\(String(describing: stackList!.value))"
          stackList = stackList!.next
        if stackList != nil { stackText += "," }
      }

      return stackText + "]"
    }
}


//TEST
let strings = DoublyLinkedList<String>()
strings.add("1j")
strings.add("2j")
strings.add("3p")
strings.add("hello world")
strings.length
print(strings.remove(node: Node<String>(value: "1j")))
print(strings)

let numbers = DoublyLinkedList<Int>()
numbers.add(4)
numbers.add(5)
numbers.add(6)
numbers.length
print(numbers)
numbers.node(index: 2) as Any


let stackString = Stack<String>()
stackString.push("first")
stackString.push("2nd")
stackString.push("3rd")
stackString.peek
stackString.pop() as Any
stackString.size
print(stackString)









